
function loadBakeries(map) {

    fetch("bakeries.json")
    .then(function(response) {
      return response.json();
    })
    .then(function(bakeries) {

        bakeries.bakeries.forEach(bakery => {

            L.marker([bakery.lat, bakery.lon]).addTo(map)
                .on('click', function(e) {
                    // Simulate a mouse click:
                    window.location.href = bakery.url;
                });
                
        });
    });
}

function main() {
    var map = L.map('map').setView([50.296, 4.035], 8);
    
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    loadBakeries(map);

  }

  

